# Run
```bash
git clone git@gitlab.com:simon_/toodoo.git
cd toodoo
docker build . --tag=toodoo
docker run -p 8000:8000 toodoo
```

# Develop
```bash
git clone git@gitlab.com:simon_/toodoo.git
cd toodoo
docker build . --tag=toodoo
docker run -p 8000:8000 -v $(pwd):/toodoo -it toodoo /bin/bash
python manage.py runserver 0:8000
```

When installing new packages with pip you must update `requirements.txt`.
```bash
pip freeze > requirements.txt
```