from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import List
from .models import ListItem


class CustomUserChangeForm(ModelForm):
    class Meta:
        model = User
        fields = ['username']


class ListForm(ModelForm):
    class Meta:
        model = List
        fields = ['text']


class ListItemForm(ModelForm):
    class Meta:
        model = ListItem
        fields = ['text']
