from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('list/<list_id>', views.display_list, name='display_list'),
    path('register', views.register, name='register'),
    path('username_change', views.username_change, name='username_change'),
    path('delete_user', views.delete_user, name='delete_user'),
    path('add_list', views.add_list, name='add_list'),
    path('modify_list/<list_id>', views.modify_list, name='modify_list'),
    path('delete_list/<list_id>', views.delete_list, name='delete_list'),
    path('add_list_item/<list_id>', views.add_list_item, name='add_list_item'),
    path('done/<list_item_id>', views.mark_list_item_done, name='done'),
    path('undo/<list_item_id>', views.mark_list_item_undone, name='undo'),
    path('modify_list_item/<list_item_id>', views.modify_list_item, name='modify_list_item'),
    path('delete_list_item/<list_item_id>', views.delete_list_item, name='delete_list_item')

]