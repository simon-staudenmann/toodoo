from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.shortcuts import redirect, render
from .models import List
from .models import ListItem
from .models import User
from . import forms


def home(request):
    if request.user.is_authenticated:
        return redirect('list/0')
    else:
        return render(request, 'core/home.html')


def register(request):
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            todo_list = List()
            user = User.objects.get(username=request.POST['username'])
            todo_list.owner = user
            todo_list.text = 'Personal'
            todo_list.save()
            messages.success(request, 'User created')
            return redirect('login')
    else:
        form = UserCreationForm()
    return render(request, 'core/register.html', {'form': form})


@login_required
def username_change(request):
    if request.method == 'POST':
        form = forms.CustomUserChangeForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, 'User updated')
            return redirect('home')
    else:
        form = forms.CustomUserChangeForm(instance=request.user)
    return render(request, 'core/username_change.html', {'form': form})


@login_required
def delete_user(request):
    user = request.user
    user.is_active = False
    user.save()
    messages.success(request, 'User deleted')
    return redirect('home')


@login_required
def add_list(request):
    if request.method == 'POST':
        todo_list = List()
        todo_list.owner = request.user
        todo_list.text = request.POST['text']
        todo_list.save()
        messages.success(request, 'List added')
    return redirect('display_list', todo_list.id)


@login_required
def modify_list(request, list_id):
    if request.method == 'POST':
        list = List.objects.get(id=list_id)
        if list.owner == request.user:
            list.text = request.POST['text']
            list.save()
            messages.success(request, 'List modified')
        else:
            messages.success(request, 'You are not allowed to modify someone else\'s List', 'danger')
    return redirect('display_list', list_id)


@login_required
def delete_list(request, list_id):
    if request.method == 'POST':
        tbd_list = List.objects.get(id=list_id)
        lists = List.objects.filter(owner=User.objects.get(username=request.user))
        if lists.count() == 1:
            messages.success(request, 'You need to have at least one list', 'danger')
            return redirect('home')
        if tbd_list.owner == request.user:
            tbd_list.delete()
            messages.success(request, 'List deleted')
        else:
            messages.success(request, 'You are not allowed to delete someone else\'s list', 'danger')
    return redirect('home')


@login_required
def add_list_item(request, list_id):
    if request.method == 'POST':
        list = List.objects.get(id=list_id)
        if list.owner == request.user:
            list_item = ListItem()
            list_item.list_id = list_id
            list_item.text = request.POST['text']
            list_item.save()
            messages.success(request, 'Task added')
        else:
            messages.success(request, 'You are not allowed to modify someone else\'s List', 'danger')
    return redirect('display_list', list_id)


@login_required
def mark_list_item_done(request, list_item_id):
    if request.method == 'POST':
        list_item = ListItem.objects.get(id=list_item_id)
        list = List.objects.get(id=list_item.list_id)
        if list.owner == request.user:
            list_item.completed = True
            list_item.save()
            messages.success(request, 'Task marked as done')
        else:
            messages.success(request, 'You are not allowed to modify someone else\'s List', 'danger')
    return redirect('display_list', list.id)


@login_required
def mark_list_item_undone(request, list_item_id):
    if request.method == 'POST':
        list_item = ListItem.objects.get(id=list_item_id)
        active_list = List.objects.get(id=list_item.list_id)
        if active_list.owner == request.user:
            list_item.completed = False
            list_item.save()
            messages.success(request, 'Task marked as undone')
        else:
            messages.success(request, 'You are not allowed to modify someone else\'s List', 'danger')
    return redirect('display_list', active_list.id)


@login_required
def modify_list_item(request, list_item_id):
    if request.method == 'POST':
        list_item = ListItem.objects.get(id=list_item_id)
        list = List.objects.get(id=list_item.list_id)
        if list.owner == request.user:
            list_item.text = request.POST['text']
            list_item.save()
            messages.success(request, 'Task modified')
        else:
            messages.success(request, 'You are not allowed to modify someone else\'s List', 'danger')
    return redirect('display_list', list.id)


@login_required
def delete_list_item(request, list_item_id):
    if request.method == 'POST':
        list_item = ListItem.objects.get(id=list_item_id)
        list = List.objects.get(id=list_item.list_id)
        if list.owner == request.user:
            list_item.delete()
            messages.success(request,'List deleted')
        else:
            messages.success(request, 'You are not allowed to modify someone else\'s List', 'danger')
    return redirect('display_list', list.id)


@login_required
def display_list(request, list_id):
    lists = List.objects.filter(owner=User.objects.get(username=request.user))
    list_items = ListItem.objects.all()
    add_list_form = forms.ListForm()
    add_list_item_form = forms.ListItemForm()
    if list_id == '0':
        return redirect('display_list', lists.order_by('-id')[0].id)
    else:
        active_list = int(list_id)
    if List.objects.get(id=list_id).owner != request.user:
        messages.success(request, 'You are not allowed to access someone else\'s to-do list', 'danger')
        return redirect('display_list', lists.order_by('-id')[0].id)
    return render(request, 'core/toodoos.html', {
        'lists': lists,
        'add_list_form': add_list_form,
        'list_items': list_items,
        'add_list_item_form': add_list_item_form,
        'active_list': active_list})
