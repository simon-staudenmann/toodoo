from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class List(models.Model):
    text = models.CharField(max_length=1000)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)


class ListItem(models.Model):
    text = models.CharField(max_length=1000)
    completed = models.BooleanField(default=False)
    list = models.ForeignKey(List, on_delete=models.CASCADE)
